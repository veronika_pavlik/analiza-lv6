using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double prvi, drugi;
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void txtB2_TextChanged(object sender, EventArgs e)
        {
            double.TryParse(txtB2.Text, out drugi);
        }

        private void btn_zbroji_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_zbroj.Text = (prvi + drugi).ToString();
        }

        private void btn_oduzmi_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_oduzmi.Text = (prvi - drugi).ToString();
        }

        private void btn_mnozi_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_umnozak.Text = (prvi * drugi).ToString();
        }

        private void btn_dijeli_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_kolicnik.Text = (prvi / drugi).ToString();
        }

        private void btn_kvad1_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_kvad1.Text = (Math.Pow(prvi,2)).ToString();
        }

        private void btn_kvad2_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_kvad2.Text = (Math.Pow(drugi,2)).ToString();
        }

        private void btn_korj1_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_korj1.Text = (Math.Sqrt(prvi)).ToString();
        }

        private void btn_korj2_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_korj2.Text = (Math.Sqrt(drugi)).ToString();
        }

        private void btn_cosx1_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_cos1.Text = (Math.Cos(prvi)).ToString();
        }

        private void btn_cosx2_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_cos2.Text = (Math.Cos(drugi)).ToString();    
        }

        private void btn_sin1_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_sin1.Text = (Math.Sin(prvi)).ToString();
        }

        private void btn_sin2_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_sin2.Text = (Math.Sin(drugi)).ToString();
        }

        private void btn_log1_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_log1.Text = (Math.Log10(prvi)).ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB1.Text, out prvi))
                MessageBox.Show("Pogre�an unos prvog broja!", "Pogre�ka");
            else if (!double.TryParse(txtB2.Text, out drugi))
                MessageBox.Show("Pogre�an unos drugog broja!", "Pogre�ka");
            else lbl_rez_log2.Text = (Math.Log10(drugi).ToString();
        }

        private void txtB1_TextChanged(object sender, EventArgs e)
        {
            double.TryParse(txtB1.Text, out prvi);
        }
    }
}